import warnings
import sys

import pandas as pd
import numpy as np
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris
import mlflow
from mlflow.models.signature import infer_signature
import mlflow.sklearn
import xgboost as xgb
from xgboost import XGBClassifier

import logging

logging.basicConfig(level=logging.WARN)
logger = logging.getLogger(__name__)

def eval_metrics(actual, pred):
    rmse = np.sqrt(mean_squared_error(actual, pred))
    mae = mean_absolute_error(actual, pred)
    r2 = r2_score(actual, pred)

    return rmse, mae, r2



if __name__ == "__main__":
    # Filter warning
    warnings.filterwarnings("ignore")
    logger.info("Starting Model")

    # Set random seed
    np.random.seed(42)

    # Load data from sklearn
    iris = load_iris()

    # Train Test Split
    X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.2, random_state=42)

    # read hyperparams from system
    eta = float(sys.argv[1]) if len(sys.argv) > 1 else 0.3
    alpha = float(sys.argv[2]) if len(sys.argv) > 2 else 0.01

    # start mlflow run
    with mlflow.start_run() as run:
        run_id = run.info.run_id

        logger.info(f"Run ID : {run_id}")

        # Create ML Model -> XGBoost Model
        bst = XGBClassifier(n_estimators=2, max_depth=2, learning_rate=eta, alpha = alpha, objective='binary:logistic')

        # Fit the Model
        bst.fit(X_train, y_train)

        # Get Test metrices
        preds = bst.predict(X_test)

        rmse, mae, r2 = eval_metrics(y_test, preds)

        # Print Them
        logger.info(f"Test RMSE : {rmse}")
        logger.info(f"Test MAE : {mae}")
        logger.info(f"Test R2 : {r2}")

        # Log Metrices with Hyperparam
        mlflow.log_param("eta", eta)
        mlflow.log_param("alpha", alpha)
        mlflow.log_param("rmse", rmse)
        mlflow.log_param("mae", mae)
        mlflow.log_param("r2", r2)

        # Get train predictions
        preds = bst.predict(X_train)

        # use them to create model signature
        signature = infer_signature(X_train, preds)

        # Log Model with pip requirements and extra pip requirements
        mlflow.xgboost.log_model(
            bst,
            "artifact_path_list",
            pip_requirements=["-r requirements.txt", "-c constraints.txt"],
            signature=signature
        )
